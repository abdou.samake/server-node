"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const app = express_1.default();
app.use(cors_1.default());
app.use(body_parser_1.default.json());
const db = firebase_admin_1.default.firestore();
const patientRefs = db.collection('patients');
const refUrines = db.collection('urines');
async function getAllUrines() {
    const urineQuerySnap = await refUrines.get();
    const urines = [];
    urineQuerySnap.forEach(urineSnap => urines.push(urineSnap.data()));
    return urines;
}
exports.getAllUrines = getAllUrines;
async function getUrineById(urineId) {
    if (!urineId) {
        throw new Error(`urineId required`);
    }
    const urineToFindRef = refUrines.doc(urineId);
    const urineToFindSnap = await urineToFindRef.get();
    if (urineToFindSnap.exists) {
        return urineToFindSnap.data();
    }
    else {
        throw new Error('object does not exists');
    }
}
exports.getUrineById = getUrineById;
async function postNewUrineData(newUrineData, patientId) {
    if (!patientId || !newUrineData) {
        throw Error('hotelId and newRoom are missing');
    }
    const refPatient = patientRefs.doc(patientId);
    const snapShotData = await refPatient.get();
    if (!snapShotData.exists) {
        return 'hotel dose not existe';
    }
    const createDataUrineRef = await refUrines.add(newUrineData);
    const createDataUrineInPatient = refPatient
        .collection('urines')
        .doc(createDataUrineRef.id);
    await createDataUrineInPatient.set(Object.assign({ ref: createDataUrineInPatient, id: createDataUrineInPatient.id }, newUrineData));
    return Object.assign(Object.assign({}, newUrineData), { id: createDataUrineInPatient.id });
}
exports.postNewUrineData = postNewUrineData;
//# sourceMappingURL=urines.service.js.map