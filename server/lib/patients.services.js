"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const app = express_1.default();
app.use(cors_1.default());
app.use(body_parser_1.default.json());
const db = firebase_admin_1.default.firestore();
const refPatients = db.collection('patients');
async function getAllPatients() {
    const patientQuerySnap = await refPatients.get();
    const patients = [];
    patientQuerySnap.forEach(patientSnap => patients.push(patientSnap.data()));
    return patients;
}
exports.getAllPatients = getAllPatients;
async function getPatientById(patientId) {
    if (!patientId) {
        throw new Error(`patientId required`);
    }
    const patientToFindRef = refPatients.doc(patientId);
    const patientToFindSnap = await patientToFindRef.get();
    if (patientToFindSnap.exists) {
        return patientToFindSnap.data();
    }
    else {
        throw new Error('object does not exists');
    }
}
exports.getPatientById = getPatientById;
async function testIfPatientExistsById(patientId) {
    const patientRef = refPatients.doc(patientId);
    const snapPatientToFind = await patientRef.get();
    const patientToFind = snapPatientToFind.data();
    if (!patientToFind) {
        throw new Error(`${patientId} does not exists`);
    }
    return patientRef;
}
async function postNewPatient(newPatient) {
    if (!newPatient) {
        throw new Error(`new patient must be filled`);
    }
    const addResult = await refPatients.add(newPatient);
    const createNewPatient = refPatients
        .doc(addResult.id);
    await createNewPatient.set(Object.assign(Object.assign({}, newPatient), { id: createNewPatient.id }));
    return Object.assign(Object.assign({}, newPatient), { id: createNewPatient.id });
}
exports.postNewPatient = postNewPatient;
async function deletePatientById(patientId) {
    if (!patientId) {
        throw new Error('patientId is missing');
    }
    const patientToDeleteRef = await testIfPatientExistsById(patientId);
    await patientToDeleteRef.delete();
    return { response: `${patientId} -> delete ok` };
}
exports.deletePatientById = deletePatientById;
async function updatePatient(patientId, newPatient) {
    if (!newPatient || !patientId) {
        throw new Error(`patient data and patient id must be filled`);
    }
    const patientToPatchRef = await testIfPatientExistsById(patientId);
    await patientToPatchRef.update(newPatient);
    return newPatient;
}
exports.updatePatient = updatePatient;
//# sourceMappingURL=patients.services.js.map