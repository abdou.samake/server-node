"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const admin = __importStar(require("firebase-admin"));
const db = admin.firestore();
const refSoignants = db.collection('soignants');
async function getAllSoignants() {
    const soignantQuerySnap = await refSoignants.get();
    const soignants = [];
    soignantQuerySnap.forEach(soignatSnap => soignants.push(soignatSnap.data()));
    return soignants;
}
exports.getAllSoignants = getAllSoignants;
async function getSoignantById(SoignantId) {
    if (!SoignantId) {
        throw new Error(`SoignantId required`);
    }
    const soignantToFindRef = refSoignants.doc(SoignantId);
    const soignantToFindSnap = await soignantToFindRef.get();
    if (soignantToFindSnap.exists) {
        return soignantToFindSnap.data();
    }
    else {
        throw new Error('object does not exists');
    }
}
exports.getSoignantById = getSoignantById;
async function testIfSoignantExistsById(soignantId) {
    const soignantRef = refSoignants.doc(soignantId);
    const snapSoignantToFind = await soignantRef.get();
    const soignantToFind = snapSoignantToFind.data();
    if (!soignantToFind) {
        throw new Error(`${soignantId} does not exists`);
    }
    return soignantRef;
}
async function postNewSoignant(newSoignant) {
    if (!newSoignant) {
        throw new Error(`new product must be filled`);
    }
    const addResult = await refSoignants.add(newSoignant);
    const createNewSoignant = refSoignants
        .doc(addResult.id);
    await createNewSoignant.set(Object.assign(Object.assign({}, newSoignant), { id: createNewSoignant.id }));
    return Object.assign(Object.assign({}, newSoignant), { id: createNewSoignant.id });
}
exports.postNewSoignant = postNewSoignant;
async function deleteSoignantById(soignantId) {
    if (!soignantId) {
        throw new Error('patientId is missing');
    }
    const soignantToDeleteRef = await testIfSoignantExistsById(soignantId);
    await soignantToDeleteRef.delete();
    return { response: `${soignantId} -> delete ok` };
}
exports.deleteSoignantById = deleteSoignantById;
async function updateSoignant(soignantId, newSoignant) {
    if (!newSoignant || !soignantId) {
        throw new Error(`soignant data and patient id must be filled`);
    }
    const soignantToPatchRef = await testIfSoignantExistsById(soignantId);
    await soignantToPatchRef.update(newSoignant);
    return newSoignant;
}
exports.updateSoignant = updateSoignant;
//# sourceMappingURL=soignant.service.js.map